import boto3
import json
import argparse
import os

parser = argparse.ArgumentParser(description="Python based command line utility to scale out and scale in EKS cluster node groups")
parser.add_argument("--scale-out",default=False,action='store_true',help='pass this flag to scale out the node group')
parser.add_argument("--scale-in",default=False,action='store_true',help='pass this flag to scale in the node group')
parser.add_argument("--config-file",default="config.json",help="path to config file")
args = parser.parse_args()


session = boto3.Session()
eks_client = session.client('eks')
autoscaling_client = session.client('autoscaling')

if not args.scale_out and not args.scale_in:
    print("No flag passed. Either pass --scale-in or --scale-out")
    os._exit(1)
if args.scale_out and args.scale_in:
    print("Both flag passed. Either pass --scale-in or --scale-out")
    os._exit(1)


config_file = open(args.config_file,"r")
clusters = json.loads(config_file.read())["list_of_clusters"]
config_file.close()

def get_capacity(scale_out_flag, scale_in_flag, cluster_name):
    if scale_out_flag:
        min_count=clusters[cluster_name]["scale_out_capacity"]["min_count"]
        desired_count=clusters[cluster_name]["scale_out_capacity"]["desired_count"]
        max_count=clusters[cluster_name]["scale_out_capacity"]["max_count"]
        print("Scaling out "+cluster_name)
    elif scale_in_flag:
        min_count=clusters[cluster_name]["scale_in_capacity"]["min_count"]
        desired_count=clusters[cluster_name]["scale_in_capacity"]["desired_count"]
        max_count=clusters[cluster_name]["scale_in_capacity"]["max_count"]
        print("Scaling in "+cluster_name)
    capacity = {
        "min_count" : min_count,
        "desired_count" : desired_count,
        "max_count" : max_count
    }
    return capacity

try:
    for cluster in clusters:
        capacity = get_capacity(args.scale_out,args.scale_in,cluster)
        nodegroups = eks_client.list_nodegroups(
            clusterName=cluster,
        )["nodegroups"]
        for nodegroup in nodegroups:
            auto_scaling_groups = eks_client.describe_nodegroup(
                clusterName=cluster,
                nodegroupName=nodegroup
            )["nodegroup"]["resources"]["autoScalingGroups"]
            for auto_scaling_group in auto_scaling_groups:
                response = autoscaling_client.update_auto_scaling_group(
                    AutoScalingGroupName=auto_scaling_group["name"],
                    MinSize = capacity["min_count"],
                    DesiredCapacity = capacity["desired_count"],
                    MaxSize = capacity["max_count"],
                )
except:
    print("Something went wrong")